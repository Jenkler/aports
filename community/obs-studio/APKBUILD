# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=obs-studio
pkgver=27.0.0
pkgrel=0
pkgdesc="Free and open source software for live streaming and screen recording"
url="https://obsproject.com/"
# mips(64), armhf and s390x blocked by vlc (librsvg)
arch="all !armhf !s390x !mips !mips64"
license="GPL-2.0-or-later"
options="!check"
makedepends="cmake ffmpeg-dev libxinerama-dev
	qt5-qtbase-dev qt5-qtx11extras-dev qt5-qtsvg-dev x264-dev fontconfig-dev
	libxcomposite-dev freetype-dev libx11-dev mesa-dev curl-dev
	pulseaudio-dev jack-dev vlc-dev alsa-lib-dev fdk-aac-dev speexdsp-dev
	v4l-utils-dev jansson-dev eudev-dev swig mbedtls-dev python3-dev
	wayland-dev pipewire-dev sndio-dev"
subpackages="$pkgname-dev"
source="https://github.com/obsproject/obs-studio/archive/$pkgver/obs-studio-$pkgver.tar.gz"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DUNIX_STRUCTURE=1 \
		-DBUILD_BROWSER=OFF \
		-DBUILD_VST=OFF
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
105da97c2adaa4a8404742368ce75ce841264268f82ae39115232dcff65245b09bb252897935fd94b305546023ef762cf8487f47e096e88ce1c27d7820f8f788  obs-studio-27.0.0.tar.gz
"
