# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=black
pkgver=21.5_beta2
_pkgver=${pkgver/_beta/b}
pkgrel=0
pkgdesc="The uncompromising Python code formatter"
url="https://github.com/psf/black"
arch="noarch !mips !mips64" # tests fail on mips
license="MIT"
depends="python3 py3-click py3-attrs py3-toml py3-appdirs py3-typed-ast py3-regex
	py3-pathspec py3-typing-extensions py3-mypy-extensions"
makedepends="py3-setuptools py3-setuptools_scm"
checkdepends="py3-pytest py3-parameterized py3-aiohttp py3-aiohttp-cors"
source="https://files.pythonhosted.org/packages/source/b/black/black-$_pkgver.tar.gz"
builddir="$srcdir/$pkgname-$_pkgver"

build() {
	python3 setup.py build
}

check() {
	# temporary installation for testing
	python3 setup.py install --root="$PWD"/test_install --skip-build

	PATH="$PWD/test_install/usr/bin:$PATH" \
		PYTHONPATH="$(echo $PWD/test_install/usr/lib/python3*/site-packages)" \
		pytest -m "not without_python2"
}

package() {
	python3 setup.py install --root="$pkgdir" --skip-build
}

sha512sums="
b9d75f48a903632b95c327946493931bf008aceee05821e5b29a5899854b8c034e7bc0eceec5ad94914339729efb295a32684eba77823eb934d5d599e6e61eab  black-21.5b2.tar.gz
"
